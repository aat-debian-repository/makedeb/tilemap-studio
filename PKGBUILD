# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
# https://github.com/Rangi42/tilemap-studio/blob/9d22719/INSTALL.md
# Seems that polished-map needs very specific software versions
pkgname=tilemap-studio
pkgver=4.0.1+git20220917.69b1d9a
pkgrel=0
_commit="69b1d9aa985c4b083294aa25cb0dc59ed5e44065"
pkgdesc='Tilemap editor for Game Boy, Color, Advance, DS, and SNES projects'
arch=("amd64")
license=("LGPL-3.0")
url="https://github.com/Rangi42/polished-map"
makedepends=("autoconf" "cmake" "libpng-dev" "libxpm-dev" "zlib1g-dev" "libx11-dev" "libxft-dev" "libxinerama-dev" "libfontconfig1-dev" "x11proto-xext-dev" "libxrender-dev" "libxfixes-dev" "libgl1-mesa-dev" "libglu1-mesa-dev" "libpulse-dev")
depends=("zlib1g" "libxpm4" "libpng16-16" "libx11-6" "libxft2" "libxinerama1" "libfontconfig1" "libxrender1" "libxfixes3" "libpulse0")
source=(
	"0001-tilemap-studio-usr-prefix.patch"
	"0002-correct-desktop-file.patch"	
	"$pkgname-$pkgver.tar.gz::https://github.com/Rangi42/tilemap-studio/archive/69b1d9a.tar.gz"
	"fltk_1.3.7.tar.gz::https://github.com/fltk/fltk/archive/refs/tags/release-1.3.7.tar.gz"
)
sha256sums=('f0b227c7ac6980c3d892b272f71ccf1b3780c9641d716c20d7348326329ee974'
            '60bc1e9738f5091e6a0da16795104c269c95a09fbe97e4e77fccc10e81cfbba0'
            'f6c2c65b7241549b671dc4d9b99dc6332e3095032353e527b31809f22a871892'
            '019f65810fb0ea5acac14c852193e8f374e822e6a3034a3c80ed8676f6f3a090')

prepare() {
	cd $pkgname-$_commit

	# Apply patches
	yes | patch -p1 < $srcdir/0001-tilemap-studio-usr-prefix.patch
	yes | patch -p1 < $srcdir/0002-correct-desktop-file.patch
}

build() {
	cd $srcdir
	# Build dependencies
	# FLTK
	cd fltk-release-1.3.7
	./autogen.sh --prefix="$PWD/.." --with-abiversion=10307
	make
	make install

	# Build main package
	# ("export PATH" is needed if bin/fltk-config is not already in your PATH)
	cd $srcdir/$pkgname-$_commit
	export PATH="../bin:$PATH"
	make
}

package() {
	cd $pkgname-$_commit
	make DESTDIR="$pkgdir" install
}


# vim: set ts=2 sw=2 et:
